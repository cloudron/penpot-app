FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg/penpot /tmp/penpot
ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

RUN apt-get -qq update && \
    apt-get -qqy install --no-install-recommends \
        netpbm poppler-utils potrace webp woff-tools woff2 fontforge gconf-service libasound2 libatk1.0-0 libatk-bridge2.0-0 \
        libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 \
        libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 \
        libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxshmfence1 libxss1 libxtst6 fonts-liberation \
        libappindicator1 libnss3 libgbm1 xvfb && \
    rm -rf /var/lib/apt/lists/*

# install openjdk
# https://github.com/penpot/penpot/blob/main/docker/images/Dockerfile.backend#L51C16-L51C80
RUN curl -LfsSo /tmp/openjdk.tar.gz https://github.com/adoptium/temurin21-binaries/releases/download/jdk-21.0.2%2B13/OpenJDK21U-jdk_x64_linux_hotspot_21.0.2_13.tar.gz && \
    echo "454bebb2c9fe48d981341461ffb6bf1017c7b7c6e15c6b0c29b959194ba3aaa5 */tmp/openjdk.tar.gz" | sha256sum -c - && \
    mkdir -p /usr/lib/jvm/openjdk && \
    cd /usr/lib/jvm/openjdk && \
    tar -xf /tmp/openjdk.tar.gz --strip-components=1 && \
    rm -rf /tmp/openjdk.tar.gz
ENV PATH="/usr/lib/jvm/openjdk/bin:$PATH" JAVA_HOME="/usr/lib/jvm/openjdk"

# remove all nodejs versions
RUN find /usr/local -maxdepth 1 -name "node-*" -type d -exec rm -rf {} \;

# install node v20
ARG NODE_VERSION=20.11.1

RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH="/usr/local/node-${NODE_VERSION}/bin:$PATH"
RUN corepack enable

ARG CLOJURE_VERSION=1.11.1.1435
RUN curl -LfsSo /tmp/clojure.sh https://download.clojure.org/install/linux-install-$CLOJURE_VERSION.sh && \
    chmod +x /tmp/clojure.sh && \
    /tmp/clojure.sh && \
    rm -rf /tmp/clojure.sh

#ARG CLJKONDO_VERSION=2024.02.12
#RUN curl -LfsSo /tmp/clj-kondo.zip https://github.com/clj-kondo/clj-kondo/releases/download/v$CLJKONDO_VERSION/clj-kondo-$CLJKONDO_VERSION-linux-amd64.zip && \
#    cd /usr/local/bin && \
#    unzip /tmp/clj-kondo.zip && \
#    rm -rf /tmp/clj-kondo.zip

# install babashka (bb) to build backend
ARG BABASHKA_VERSION=1.3.189
RUN curl -LfsSo /tmp/babashka.tar.gz https://github.com/babashka/babashka/releases/download/v$BABASHKA_VERSION/babashka-$BABASHKA_VERSION-linux-amd64.tar.gz && \
    cd /usr/local/bin && \
    tar -xf /tmp/babashka.tar.gz && \
    rm -rf /tmp/babashka.tar.gz

ARG CLJFMT_VERSION=0.12.0
RUN curl -LfsSo /tmp/cljfmt.tar.gz https://github.com/weavejester/cljfmt/releases/download/${CLJFMT_VERSION}/cljfmt-${CLJFMT_VERSION}-linux-amd64.tar.gz && \
    cd /usr/local/bin && \
    tar -xf /tmp/cljfmt.tar.gz && \
    rm -rf /tmp/cljfmt.tar.gz

# even though penpot makes releases, it doesn't make one for every tag
# renovate: datasource=github-tags depName=penpot/penpot versioning=semver
ARG PENPOT_VERSION=2.5.1

# ARG CURRENT_VERSION=$VERSION # is this required?

RUN export BUILD_DATE=$(date -R)

RUN wget https://github.com/penpot/penpot/archive/refs/tags/${PENPOT_VERSION}.tar.gz -O - | \
    tar -xz --strip-components 1 -C /tmp/penpot

# build exporter
WORKDIR /tmp/penpot/exporter
ENV NODE_ENV=develop
RUN yarn install
ENV NODE_ENV=production
#RUN mkdir -p /app/code/browsers
#ENV PLAYWRIGHT_BROWSERS_PATH=/app/code/browsers

RUN clojure -J-Xms100M -J-Xmx1000M -J-XX:+UseSerialGC -M:dev:shadow-cljs release main && \
    rm -rf target/app && \
    cp ../.yarnrc.yml target/ && \
    cp package.json target/ && \
    sed -i -re "s/\%version\%/$PENPOT_VERSION/g" ./target/app.js && \
    yarn run playwright install chromium && \
    mv ./target /app/code/exporter && \
    mv node_modules /app/code/exporter

RUN echo $PENPOT_VERSION > /app/code/exporter/version.txt

# build backend bundle (see /manage.sh, backend/scripts/build)
WORKDIR /tmp/penpot/backend
RUN mkdir -p target/classes && \
    mkdir -p target/dist && \
    echo "$PENPOT_VERSION" > target/classes/version.txt && \
    cp ../CHANGES.md target/classes/changelog.md && \
    clojure -T:build jar && \
    mv target/penpot.jar target/dist/penpot.jar && \
    cp resources/log4j2.xml target/dist/log4j2.xml && \
    cp scripts/manage.py target/dist/manage.py && \
    chmod +x target/dist/manage.py && \
    bb ./scripts/prefetch-templates.clj resources/app/onboarding.edn builtin-templates/ && \
    cp -r builtin-templates target/dist/

# Prefetch templates
RUN rm -rf builtin-templates && \
    mkdir builtin-templates && \
    bb ./scripts/prefetch-templates.clj resources/app/onboarding.edn builtin-templates/ && \
    cp -r builtin-templates target/dist/

RUN mv ./target/dist /app/code/backend

ENV NODE_ENV=production

# Install Rust toolchain
# https://github.com/penpot/penpot/blob/d500058aa9dc1680f340fe2d310d8a3a9333ef01/docker/devenv/Dockerfile#L248
ARG RUSTUP_VERSION=1.27.1
ARG RUST_VERSION=1.82.0

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH;

# Same steps as in Rust official Docker image https://github.com/rust-lang/docker-rust/blob/9f287282d513a84cb7c7f38f197838f15d37b6a9/1.81.0/bookworm/Dockerfile
RUN rustArch='x86_64-unknown-linux-gnu' && \
    rustupSha256='6aeece6993e902708983b209d04c0d1dbb14ebb405ddb87def578d41f920f56d' && \
    url="https://static.rust-lang.org/rustup/archive/${RUSTUP_VERSION}/${rustArch}/rustup-init" && \
    wget "$url" && \
    echo "${rustupSha256} *rustup-init" | sha256sum -c - && \
    chmod +x rustup-init && \
    ./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION --default-host ${rustArch} && \
    rm rustup-init && \
    chmod -R a+w $RUSTUP_HOME $CARGO_HOMEi && \
    rustup component add rustfmt;

WORKDIR /usr/local

# Install emscripten SDK and activate it
RUN git clone https://github.com/emscripten-core/emsdk.git && \
    cd emsdk && \
    ./emsdk install latest && \
    ./emsdk activate latest && \
    rustup target add wasm32-unknown-emscripten

# build frontend bundle (see ./manage.sh, frontend/scripts/build)
WORKDIR /tmp/penpot/frontend

RUN yarn install && \
    rm -rf resources/public && \
    rm -rf target/dist && \
    yarn run build:app:main --config-merge "{:release-version \"${PENPOT_VERSION}\"}" $EXTRA_PARAMS && \
    yarn run build:wasm && \
    yarn run build:app:libs && \
    yarn run build:app:assets && \
    mkdir -p target/dist && \
    rsync -avr resources/public/ target/dist/ && \
    sed -i -re "s/\%version\%/$PENPOT_VERSION/g" ./target/dist/index.html && \
    sed -i -re "s/\%buildDate\%/$BUILD_DATE/g" ./target/dist/index.html && \
    sed -i -re "s/\%version\%/$PENPOT_VERSION/g" ./target/dist/render.html && \
    sed -i -re "s/\%version\%/$PENPOT_VERSION/g" ./target/dist/rasterizer.html && \
    sed -i -re "s/\%buildDate\%/$BUILD_DATE/g" ./target/dist/index.html && \
    sed -i -re "s/\%buildDate\%/$BUILD_DATE/g" ./target/dist/rasterizer.html && \
    sed -i "s/version=develop/version=$CURRENT_VERSION/g" ./target/dist/js/render_wasm.js && \
    mv ./target/dist /app/code/frontend

RUN echo $PENPOT_VERSION > /app/code/frontend/version.txt
RUN ln -sf /run/config.js /app/code/frontend/js/config.js

RUN echo $PENPOT_VERSION > /app/code/backend/version.txt

WORKDIR /app/code

# add nginx config
RUN rm /etc/nginx/sites-enabled/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    rm -rf /var/lib/nginx/ && \
    mkdir -p /run/nginx/tmp && \
    ln -sf /run/nginx /var/lib/nginx

# https://github.com/penpot/penpot/blob/main/docker/images/Dockerfile.frontend#L14
# https://github.com/penpot/penpot/tree/main/docker/images/files
COPY nginx/penpot-nginx.conf /etc/nginx/nginx.conf
COPY nginx/nginx-mime.types /etc/nginx/mime.types

# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/penpot/supervisord.log /var/log/supervisor/supervisord.log

RUN chown -R cloudron:cloudron /app/code

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
