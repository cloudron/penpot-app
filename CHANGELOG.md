[0.1.0]
* Initial version for Penpot

[0.2.0]
* Update Penpot to  1.18.4
* [Full changelog](https://github.com/penpot/penpot/releases/tag/1.18.4)
* Fix zooming while color picker breaks UI GH #3214
* Fix problem with layout not reflowing on shape deletion Taiga #5289
* Fix extra long typography names on assets and palette Taiga #5199
* Fix background-color property on inspect code Taiga #5300
* Preview layer blend modes (by @akshay-gupta7) Github #3235

[0.3.0]
* Improve healthcheck

[0.4.0]
* Update Penpot to 1.18.5
* [Full changelog](https://github.com/penpot/penpot/releases/tag/1.18.5)
* Fix add flow option in contextual menu for frames

[0.5.0]
* Update Penpot to 1.19.0
* [Full changelog](https://github.com/penpot/penpot/releases/tag/1.19.0)

[1.0.0]
* First stable app package release with Penpot 1.19.0

[1.0.1]
* Update Penpot to 1.19.1
* [Full changelog](https://github.com/penpot/penpot/compare/1.19.0...1.19.1)

[1.0.2]
* Update Penpot to 1.19.2
* [Full changelog](https://github.com/penpot/penpot/compare/1.19.1...1.19.2)

[1.0.3]
* Update Penpot to 1.19.3
* [Full changelog](https://github.com/penpot/penpot/compare/1.19.2...1.19.3)
* Improve layers multiselection behaviour

[1.0.4]
* Fix external font usage

[1.0.5]
* Fix nginx assets data location on disk for imported images

[1.0.6]
* Update Penpot to 1.19-hotfix-33
* [Full changelog](https://github.com/penpot/penpot/releases/tag/1.19-hotfix-33)
* Fix incorrect props handling on profile registration

[1.1.0]
* Enable password login for users which were invited

[1.2.0]
* Update Penpot to 2.0.2
* [Full changelog](https://github.com/penpot/penpot/releases/tag/2.0.2)

[1.2.1]
* Update Penpot to 2.0.3
* [Full changelog](https://github.com/penpot/penpot/releases/tag/2.0.3)

[1.3.0]
* Add email display name support

[1.4.0]
* Update Penpot to 2.1.0
* [Full changelog](https://github.com/penpot/penpot/releases/tag/2.1.0)

[1.4.1]
* Update Penpot to 2.1.1
* [Full changelog](https://github.com/penpot/penpot/releases/tag/2.1.1)

[1.4.2]
* Update Penpot to 2.1.2
* [Full changelog](https://github.com/penpot/penpot/releases/tag/2.1.2)

[1.4.3]
* Update Penpot to 2.1.3
* [Full changelog](https://github.com/penpot/penpot/releases/tag/2.1.3)
* Fix deleted fonts on file load

[1.4.4]
* Update Penpot to 2.1.4
* [Full changelog](https://github.com/penpot/penpot/releases/tag/2.1.4)

[1.5.0]
* Update Penpot to 2.2.0
* [Full changelog](https://github.com/penpot/penpot/releases/tag/2.2.0)


[1.5.1]
* Update Penpot to 2.3.0
* Fix problem when translating multiple path points by [@&#8203;eeropic](https://github.com/eeropic) [#&#8203;4459](https://github.com/penpot/penpot/issues/4459)
* **Replace Draft.js completely with a custom editor** [Taiga #&#8203;7706](https://tree.taiga.io/project/penpot/us/7706)
* Fix problem with constraints buttons [Taiga #&#8203;8465](https://tree.taiga.io/project/penpot/issue/8465)
* Fix problem with go back button on error page [Taiga #&#8203;8887](https://tree.taiga.io/project/penpot/issue/8887)
* Fix problem with shadows in text for Safari [Taiga #&#8203;8770](https://tree.taiga.io/project/penpot/issue/8770)
* Fix a regression with feedback form subject and content limits [Taiga #&#8203;8908](https://tree.taiga.io/project/penpot/issue/8908)
* Fix problem with stroke and filter ordering in frames [Github #&#8203;5058](https://github.com/penpot/penpot/issues/5058)
* Fix problem with hover layers when hidden/blocked [Github #&#8203;5074](https://github.com/penpot/penpot/issues/5074)
* Fix problem with precision on boolean calculation [Taiga #&#8203;8482](https://tree.taiga.io/project/penpot/issue/8482)
* Fix problem when translating multiple path points [Github #&#8203;4459](https://github.com/penpot/penpot/issues/4459)
* Fix problem on importing (and exporting) files with flows [Taiga #&#8203;8914](https://tree.taiga.io/project/penpot/issue/8914)
* Fix Internal Error page: "go to your penpot" wrong design [Taiga #&#8203;8922](https://tree.taiga.io/project/penpot/issue/8922)
* Fix problem updating layout when toggle visibility in component copy [Github #&#8203;5143](https://github.com/penpot/penpot/issues/5143)
* Fix "Done" button on toolbar on inspect mode should go to design mode [Taiga #&#8203;8933](https://tree.taiga.io/project/penpot/issue/8933)
* Fix problem with shortcuts in text editor [Github #&#8203;5078](https://github.com/penpot/penpot/issues/5078)
* Fix problems with show in viewer and interactions [Github #&#8203;4868](https://github.com/penpot/penpot/issues/4868)
* Add visual feedback when moving an element into a board [Github #&#8203;3210](https://github.com/penpot/penpot/issues/3210)
* Fix percent calculation on grid layout tracks [Github #&#8203;4688](https://github.com/penpot/penpot/issues/4688)
* Fix problem with caps and inner shadows [Github #&#8203;4517](https://github.com/penpot/penpot/issues/4517)
* Fix problem with horizontal/vertical lines and shadows [Github #&#8203;4516](https://github.com/penpot/penpot/issues/4516)
* Fix problem with layers overflowing panel [Taiga #&#8203;9021](https://tree.taiga.io/project/penpot/issue/9021)
* Fix in workspace you can manage rulers on view mode [Taiga #&#8203;8966](https://tree.taiga.io/project/penpot/issue/8966)
* Fix problem with swap components in grid layout [Taiga #&#8203;9066](https://tree.taiga.io/project/penpot/issue/9066)

[1.5.2]
* Update penpot to 2.3.3
* [Full Changelog](https://github.com/penpot/penpot/releases/tag/2.2.0)
* Fix problem creating manual overlay interactions [Taiga #&#8203;9146](https://tree.taiga.io/project/penpot/issue/9146)
* Fix null pointer exception on number checking functions
* Fix problem with grid layout ordering after moving [Taiga #&#8203;9179](https://tree.taiga.io/project/penpot/issue/9179)
* Add initial documentation for Kubernetes
* Fix unexpected issue on interaction between plugins sandbox and

[1.6.0]
* Update penpot to 2.4.1
* [Full Changelog](https://github.com/penpot/penpot/releases/tag/2.4.1)
[1.6.1]
* Update penpot to 2.4.2
* [Full Changelog](https://github.com/penpot/penpot/releases/tag/2.4.2)
* Fix detach when top copy is dangling and nested copy is not [Taiga #&#8203;9699](https://tree.taiga.io/project/penpot/issue/9699)
* Fix problem in plugins with `replaceColor` method [#&#8203;174](https://github.com/penpot/penpot-plugins/issues/174)
* Fix issue with recursive commponents [Taiga #&#8203;9903](https://tree.taiga.io/project/penpot/issue/9903)
* Fix missing methods reference on API Docs
* Fix memory usage issue on file-gc asynchronous task (related to snapshots feature)

[1.6.2]
* Update penpot to 2.4.3
* [Full Changelog](https://github.com/penpot/penpot/releases/tag/2.4.3)
* Fix errors from editable select on measures menu [Taiga #&#8203;9888](https://tree.taiga.io/project/penpot/issue/9888)
* Fix exception on importing some templates from templates slider
* Consolidate adding share button to workspace
* Fix problem when pasting text [Taiga #&#8203;9929](https://tree.taiga.io/project/penpot/issue/9929)
* Fix incorrect media reference handling on component instantiation

[1.7.0]
* Update penpot to 2.5.1
* [Full Changelog](https://github.com/penpot/penpot/releases/tag/2.5.1)
* Improve Nginx entryponit to get the resolvers dinamically by default

