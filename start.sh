#!/bin/bash

set -eu

mkdir -p /run/penpot /app/data/assets

export PENPOT_BACKEND_FLAGS="disable-onboarding enable-registration disable-login-with-password disable-email-verification enable-smtp enable-login-with-oidc"
export PENPOT_FRONTEND_FLAGS="disable-onboarding disable-registration disable-login-with-password disable-email-verification enable-smtp enable-login-with-oidc"

[[ ! -f /app/data/.secret_key ]] && pwgen -1 32 > /app/data/.secret_key

export NODE_ENV=production

cat > /run/penpot/env.sh <<EOT
## You can read more about all available flags and other
## environment variables for the backend here:
## https://help.penpot.app/technical-guide/configuration/#advanced-configuration
export PENPOT_FLAGS="${PENPOT_BACKEND_FLAGS}"

export PENPOT_PUBLIC_URI=${CLOUDRON_APP_ORIGIN}
#export PENPOT_HTTP_SERVER_PORT=6060
#export PENPOT_HTTP_SERVER_HOST=localhost
export PENPOT_REDIS_URI="redis://${CLOUDRON_REDIS_HOST}/0"

export PENPOT_SECRET_KEY=$(cat /app/data/.secret_key)

export PENPOT_DATABASE_URI="postgresql://${CLOUDRON_POSTGRESQL_HOST}/${CLOUDRON_POSTGRESQL_DATABASE}"
export PENPOT_DATABASE_USERNAME=${CLOUDRON_POSTGRESQL_USERNAME}
export PENPOT_DATABASE_PASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD}

export PENPOT_ASSETS_STORAGE_BACKEND=assets-fs
export PENPOT_STORAGE_ASSETS_FS_DIRECTORY=/app/data/assets

## Also can be configured to to use a S3 compatible storage
## service like MiniIO. Look below for minio service setup.

# - AWS_ACCESS_KEY_ID=<KEY_ID>
# - AWS_SECRET_ACCESS_KEY=<ACCESS_KEY>
# - PENPOT_ASSETS_STORAGE_BACKEND=assets-s3
# - PENPOT_STORAGE_ASSETS_S3_ENDPOINT=http://penpot-minio:9000
# - PENPOT_STORAGE_ASSETS_S3_BUCKET=<BUKET_NAME>

# OIDC
# CLOUDRON_OIDC_PROVIDER_NAME is not supported
export PENPOT_OIDC_BASE_URI="${CLOUDRON_OIDC_ISSUER}"
export PENPOT_OIDC_CLIENT_ID="${CLOUDRON_OIDC_CLIENT_ID}"
export PENPOT_OIDC_CLIENT_SECRET="${CLOUDRON_OIDC_CLIENT_SECRET}"
export PENPOT_OIDC_SCOPES="openid profile email"

# Optional list of roles that users are required to have. If no role
# is provided, roles checking  disabled.
#export PENPOT_OIDC_ROLES="role1 role2"

# SMTP/Email configuration.
# https://help.penpot.app/technical-guide/configuration/#email-(smtp)
export PENPOT_SMTP_DEFAULT_FROM="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Penpot} <${CLOUDRON_MAIL_FROM}>"
export PENPOT_SMTP_DEFAULT_REPLY_TO="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Penpot} <${CLOUDRON_MAIL_FROM}>"
export PENPOT_SMTP_HOST=${CLOUDRON_MAIL_SMTP_SERVER}
export PENPOT_SMTP_PORT=${CLOUDRON_MAIL_SMTP_PORT}
export PENPOT_SMTP_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}
export PENPOT_SMTP_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}
export PENPOT_SMTP_TLS=false
export PENPOT_SMTP_SSL=false

EOT
source /run/penpot/env.sh

cat > /run/config.js <<EOT
var penpotFlags = "${PENPOT_FRONTEND_FLAGS}";
EOT

echo "=> Ensure permissions"
#chown -R cloudron:cloudron /app/data /run/
chmod a+x /run/penpot/env.sh

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Penpot
