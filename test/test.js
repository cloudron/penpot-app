#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.EMAIL || !process.env.PASSWORD) {
    console.log('EMAIL, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;
    const username = process.env.EMAIL;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password, { clickLogin, createAccount }) {
        await browser.manage().deleteAllCookies();

        await browser.get(`https://${app.fqdn}/#/auth/login`);
        await waitForElement(By.xpath('//a[.="OpenID"]'));

        // await browser.findElement(By.xpath('//a[contains(@class, "btn-github-auth") and contains(text(), "OpenID")]')).click();
        await browser.findElement(By.xpath('//a[.="OpenID"]')).click();
        await browser.sleep(2000);

        if (clickLogin) {
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(4000);
        }

        if (createAccount) {
            await browser.findElement(By.xpath('//button[@label="Create an account"]')).click();
        }

        await waitForElement(By.xpath('//h1[contains(text(), "Projects")]'));
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//h1[contains(text(), "Projects")]'));
    }

    async function createProject() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(5000);

        await waitForElement(By.xpath('//button[@data-testid="new-project-button" or @data-test="new-project-button"]'));
        await browser.findElement(By.xpath('//button[@data-testid="new-project-button" or @data-test="new-project-button"]')).click();
        await waitForElement(By.xpath('//h1[contains(text(), "Projects")]'));
        await browser.findElement(By.xpath('//h1[contains(text(), "Projects")]')).click();
        await browser.sleep(2000);
    }

    async function checkProject() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//h2[@title="New Project 1"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);     });
    it('wait for all services', async function () { await sleep(20000); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password, { clickLogin: true, createAccount: true }));
    it('can get the main page', getMainPage);

    it('can create project', createProject);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login.bind(null, username, password, { clickLogin: false, createAccount: false }));

    it('check Project', checkProject);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password, { clickLogin: false, createAccount: false }));
    it('can get the main page', getMainPage);

    it('check Project', checkProject);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password, { clickLogin: false, createAccount: false }));
    it('can get the main page', getMainPage);
    it('check Project', checkProject);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id app.penpot.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('wait for all services', async function () { await sleep(20000); });

    it('clear the browser history', clearCache);
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password, { clickLogin: true, createAccount: true }));
    it('can get the main page', getMainPage);

    it('can create project', createProject);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login.bind(null, username, password, { clickLogin: false, createAccount: false }));
    it('check Project', checkProject);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
